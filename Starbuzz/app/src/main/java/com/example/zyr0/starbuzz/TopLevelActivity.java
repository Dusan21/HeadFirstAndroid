package com.example.zyr0.starbuzz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

public class TopLevelActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top_level);

        Toast toast = Toast.makeText(this, "Not Implemented" ,Toast.LENGTH_LONG);
        AdapterView.OnItemClickListener itemClickListener = (listView, itemView, position, id) -> {
            switch (position){
                case 0:
                    Intent intent = new Intent(this, DrinkCategoryActivity.class);
                    startActivity(intent);
                    break;
                case 1:
                    toast.show();
                    break;
                case 2:
                    toast.show();
                    break;
            }
        };

        ListView listView = findViewById(R.id.list_options);
        listView.setOnItemClickListener(itemClickListener);
    }

}
