package com.example.zyr0.beeradviser;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.List;

public class FindBearActivity extends Activity {
    private BeerExpert beerExpert = new BeerExpert();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_bear);
    }

    public void onClickFindBeer(View view){
        final TextView brands = findViewById(R.id.brands);
        Spinner color = findViewById(R.id.color);
        String beerColor = String.valueOf(color.getSelectedItem());

        List<String> brandsList = beerExpert.getBrands(beerColor);
        final StringBuilder brandsFormatted = new StringBuilder();

        for (String brand : brandsList) {
            brandsFormatted.append(brand).append("\n");
        }

        brands.setText(brandsFormatted);
    }
}
