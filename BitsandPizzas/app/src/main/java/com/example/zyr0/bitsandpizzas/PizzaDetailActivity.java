package com.example.zyr0.bitsandpizzas;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

public class PizzaDetailActivity extends AppCompatActivity {

    public static final String EXTRA_PIZZA_ID = "pizzaId";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pizza_detail);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        int pizzaId = getIntent().getExtras().getInt(EXTRA_PIZZA_ID);

        String pizzaName = Pizza.pizzas[pizzaId].getName();
        TextView text = findViewById(R.id.pizza_text);
        text.setText(pizzaName);

        int pizzaImage = Pizza.pizzas[pizzaId].getImageResourceId();
        ImageView image = findViewById(R.id.pizza_image);
        image.setImageDrawable(ContextCompat.getDrawable(this, pizzaImage));
        image.setContentDescription(pizzaName);
    }


}
