package com.example.zyr0.workout;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

public class DetailActivity extends AppCompatActivity {

    static final String EXTRA_WORKOUT_ID = "id";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        WorkoutDetailsFragment frag = (WorkoutDetailsFragment)
                getSupportFragmentManager().findFragmentById(R.id.detail_flag);

        if (frag != null){
            int id = (int) getIntent().getExtras().get(EXTRA_WORKOUT_ID);
            frag.setWorkoutId(id);
        }

        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.setDisplayHomeAsUpEnabled(true);
        }

    }
}
